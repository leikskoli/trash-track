extends CharacterBody2D
class_name Car

var player: Player
var turn_speed: float = 5.0 # radians / sec.
var move_speed: float = 200 # px/s.

# Input Map Strings.
var move_left: String
var move_right: String
var move_up: String
var move_down: String
#var use: String


func _ready():
	if player.index >= 0:
		move_left = "p" + str(player.index) + "_move_left"
		move_right = "p" + str(player.index) + "_move_right"
		move_up = "p" + str(player.index) + "_move_up"
		move_down = "p" + str(player.index) + "_move_down"
		#use = "p" + str(index) + "_use"


func _physics_process(delta):
	var direction = Input.get_vector(move_left, move_right, move_up, move_down)
	# If the player is trying to move.
	if direction != Vector2.ZERO:
		if NearestAngle(rotation, direction.angle()) >= 0:
			rotation += turn_speed * delta
		else:
			rotation -= turn_speed * delta

		# Unwind as we go over the bounds.
		#if current_angle < -PI:
			#current_angle += 2 * PI
		#elif current_angle > PI:
			#current_angle -= 2 * PI
		#
		#arm.rotation = current_angle
		
		#print(direction.angle())
		#rotation += direction.angle() * turn_rate * delta
		velocity = direction * move_speed
		move_and_slide()
		
		for i in get_slide_collision_count():
			var c = get_slide_collision(i)
			if c.get_collider() is Trash:
				c.get_collider().queue_free()
				#c.get_collider().apply_central_impulse(-c.get_normal() * push_force)


static func NearestAngle(angle_rad_a: float, angle_rad_b: float):
	var distance: float = fmod(angle_rad_b - angle_rad_a, 2 * PI) # 360°
	if distance < -PI:  # -180°
		distance += 2 * PI  # 360°
	elif distance > PI: # 180°
		distance -= 2 * PI  # 360°
	return distance
