extends PanelContainer
class_name Player

const CAR = preload("res://Car/Car.tscn")
@onready var score_ui = $VBox/ScoreUI

var index: int = -1 # Set to valid number at spawn.
var score: int = 0
var car: Car

func _ready():
	UpdateSore(0)
	SpawnCar()

func UpdateSore(delta_score: int):
	score += delta_score
	score_ui.text = str(score)

func ChooseCar():
	pass

func SpawnCar():
	var car = CAR.instantiate()
	car.player = self
	car.position = Vector2(100, 100)
	get_parent().get_parent().add_child(car)
