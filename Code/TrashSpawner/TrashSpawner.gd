extends Node2D
class_name TrashSpawner

const TRASH = preload("res://Trash/Trash.tscn")
@onready var timer = $Timer

@export var countdown: float = 5.0

func _ready():
	timer.wait_time = countdown
	timer.timeout.connect(OnSpawnTrash)


func OnSpawnTrash():
	var trash = TRASH.instantiate()
	add_child(trash)

