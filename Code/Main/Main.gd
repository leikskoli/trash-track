extends Node
class_name Main

const PLAYER = preload("res://Player/Player.tscn")
@onready var players_node = $PlayersNode

var players: Array[Player]


func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed and event.keycode == KEY_ESCAPE:
			get_tree().quit()


func _process(delta):
	if Input.is_action_just_pressed("p0_spawn"):
		SpawnPlayer(0)
		print("Spawned")
	if Input.is_action_just_pressed("p1_spawn"):
		SpawnPlayer(1)
	if Input.is_action_just_pressed("p2_spawn"):
		SpawnPlayer(2)
	if Input.is_action_just_pressed("p3_spawn"):
		SpawnPlayer(3)


func SpawnPlayer(index: int):
	for player in players:
		if player.index == index:
			return
	
	var player = PLAYER.instantiate()
	player.index = index
	#player.position = rig.position + Vector2(200, 200)
	players.append(player)
	players_node.add_child(player)
